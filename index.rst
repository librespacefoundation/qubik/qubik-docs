QUBIK Mission and Bus documentation
###################################

.. toctree::
   :maxdepth: 2
   :caption: QUBIK Mission
   :hidden:

   introduction
   objectives
   experiments

.. toctree::
   :maxdepth: 2
   :caption: QUBIK Ground Station
   :hidden:

   reception
   command

.. toctree::
   :maxdepth: 2
   :caption: QUBIK Budgets
   :hidden:

   budget

.. toctree::
   :maxdepth: 2
   :caption: QUBIK Bus Components
   :hidden:

   comms-obc
   structural
   battery-management-power-system
   solar-power-board
   cabling

.. toctree::
   :maxdepth: 2
   :caption: QUBIK Bus Assembly
   :hidden:

   pre-assembly
   Assembly Guide <https://qubik.libre.space/projects/qubik-assembly-guide>

.. toctree::
   :maxdepth: 2
   :caption: QUBIK ICD
   :hidden:

   user-manual
   interface

.. toctree::
   :maxdepth: 2
   :caption: QUBIK Testing
   :hidden:

   environmental-functional-testing

* :ref:`search`
